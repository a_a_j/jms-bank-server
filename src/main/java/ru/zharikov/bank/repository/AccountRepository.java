package ru.zharikov.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.zharikov.bank.model.Account;

@Repository("accountRepository")
public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findByNumber(int number);
    Account save(Account account);
}
