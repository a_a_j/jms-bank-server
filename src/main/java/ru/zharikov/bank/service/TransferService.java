package ru.zharikov.bank.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zharikov.bank.model.Account;
import ru.zharikov.bank.model.Transfer;
import ru.zharikov.bank.repository.AccountRepository;
import ru.zharikov.bank.repository.TransferRepository;

@Service("transferService")
@RequiredArgsConstructor
public class TransferService {
    private final TransferRepository transferRepository;
    private final AccountRepository accountRepository;

    public Transfer send(Transfer transfer) {
        transfer.setStatus(0);
        Account senderAccount = accountRepository.findByNumber(transfer.getSender());
        Account recipientAccount = accountRepository.findByNumber(transfer.getRecipient());
        if (senderAccount != null && senderAccount.getActive() == 1 &&
                recipientAccount != null && recipientAccount.getActive() == 1 &&
                senderAccount.getBalance() >= transfer.getAmount()) {
            senderAccount.setBalance(senderAccount.getBalance() - transfer.getAmount());
            recipientAccount.setBalance(recipientAccount.getBalance() + transfer.getAmount());
            accountRepository.save(senderAccount);
            accountRepository.save(recipientAccount);
            transfer.setStatus(1);
            transfer = transferRepository.save(transfer);
        }
        return transfer;
    }
}
