package ru.zharikov.bank.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zharikov.bank.model.Account;
import ru.zharikov.bank.repository.AccountRepository;

import java.util.concurrent.ThreadLocalRandom;

@Service("accountService")
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;

    public Account findByNumber(int number){
        return accountRepository.findByNumber(number);
    }

    public Account save(Account account) {
        if (account.getNumber() == 0) {
            // рандомайзер генерит номер новому счету
            account.setNumber(ThreadLocalRandom.current().nextInt(100, 999));
            account.setActive(1);
        }
        return accountRepository.save(account);
    }

    public Account recharge(Account account) {
        Account actualAccount = accountRepository.findByNumber(account.getNumber());
        if (actualAccount != null) {
            actualAccount.setBalance(actualAccount.getBalance() + account.getBalance());
            accountRepository.save(actualAccount);
        }
        return actualAccount;
    }

    public Account withdraw(Account account) {
        Account actualAccount = accountRepository.findByNumber(account.getNumber());
        if (actualAccount != null && actualAccount.getBalance() >= account.getBalance()) {
            actualAccount.setBalance(actualAccount.getBalance() - account.getBalance());
            accountRepository.save(actualAccount);
        }
        return actualAccount;
    }

    public Account close(Account account) {
        Account actualAccount = accountRepository.findByNumber(account.getNumber());
        if (actualAccount != null) {
            actualAccount.setActive(0);
            accountRepository.save(actualAccount);
        }
        return actualAccount;
    }
}
