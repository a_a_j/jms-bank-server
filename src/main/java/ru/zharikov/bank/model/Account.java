package ru.zharikov.bank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private int id;

    @Column
    private String firstName;

    @Column
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(unique = true)
    private int number;

    @Column
    private double balance;

    @Column
    private int active;
}
