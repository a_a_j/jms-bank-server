package ru.zharikov.bank.integration;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import ru.zharikov.bank.model.Account;
import ru.zharikov.bank.service.AccountService;

import javax.jms.*;
import java.util.Enumeration;

@Component("accountListener")
public class AccountListener {
    private static final String ACCOUNT_REQUEST = "account.in";
    private static final String ACCOUNT_RESPONSE = "account.out";
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountListener.class);
    private AccountService accountService;

    @Autowired
    public AccountListener(AccountService accountService) {
        this.accountService = accountService;
    }

    @JmsListener(destination = ACCOUNT_REQUEST)
    @SendTo(ACCOUNT_RESPONSE)
    public String receiveQueueMessage(final Message message) {
        Account account = null;
        Gson gson = new Gson();
        String value;
        MapMessage mapMessage = (MapMessage) message;
        try {
            Enumeration enumeration = mapMessage.getMapNames();
            String operation = (String) enumeration.nextElement();
            value = mapMessage.getString(operation);
            switch (operation) {
                case "find":
                    account = accountService.findByNumber(Integer.parseInt(value));
                    break;
                case "save":
                    account = gson.fromJson(value, Account.class);
                    account = accountService.save(account);
                    break;
                case "recharge":
                    account = gson.fromJson(value, Account.class);
                    account = accountService.recharge(account);
                    break;
                case "withdraw":
                    account = gson.fromJson(value, Account.class);
                    account = accountService.withdraw(account);
                    break;
                case "close":
                    account = gson.fromJson(value, Account.class);
                    account = accountService.close(account);
                    break;
                default:
                    break;
            }
        } catch (JMSException e) {
            LOGGER.warn("Error on sending message", e);
        }
        return gson.toJson(account);
    }

}
