package ru.zharikov.bank.integration;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import ru.zharikov.bank.model.Transfer;
import ru.zharikov.bank.service.TransferService;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import java.util.Enumeration;

@Component("transferListener")
public class TransferListener {
    private static final String TRANSFER_REQUEST = "transfer.in";
    private static final String TRANSFER_RESPONSE = "transfer.out";
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountListener.class);
    private TransferService transferService;

    @Autowired
    public TransferListener(TransferService transferService) {
        this.transferService = transferService;
    }

    @JmsListener(destination = TRANSFER_REQUEST)
    @SendTo(TRANSFER_RESPONSE)
    public String receiveQueueMessage(final Message message) {
        Transfer transfer = null;
        Gson gson = new Gson();
        String value;
        MapMessage mapMessage = (MapMessage) message;
        try {
            Enumeration enumeration = mapMessage.getMapNames();
            String operation = (String) enumeration.nextElement();
            if (operation.equals("save")) {
                value = mapMessage.getString("save");
                transfer = gson.fromJson(value, Transfer.class);
                transfer = transferService.send(transfer);
            }
        } catch (JMSException e) {
            LOGGER.warn("Error on sending message", e);
        }
        return gson.toJson(transfer);
    }
}
